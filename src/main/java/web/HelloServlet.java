package web;

import Warmup_11_22_2019.jdbc.DbConnection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class HelloServlet extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    PrintWriter writer = resp.getWriter();
ConnectionApp connectionApp =new ConnectionApp();
      ResultSet rset = null;
      try {
          rset = connectionApp.getData();

      while(rset.next()) {
      String line = String.format("id: %s, sender:%s, receiver:%s, message:%s",
              rset.getString("a"),
              rset.getString("operation"),
              rset.getString("b"),
              rset.getString("result"));
writer.printf(line);
      writer.println();
    }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    writer.close();
  }
}
