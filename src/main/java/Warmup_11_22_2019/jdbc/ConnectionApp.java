package Warmup_11_22_2019.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ConnectionApp {
  public static void main(String[] args) throws SQLException {
    Connection conn = DbConnection.getConnection();

    final String SQLS = "SELECT * FROM messages";
    final String SQLS2 = "SELECT * FROM student";
    final  String SQLI= "INSERT INTO messages (content, user_from, user_to) values('aaaa',1,2)";

    PreparedStatement stmt = conn.prepareStatement(SQLS);
    PreparedStatement stmt2 = conn.prepareStatement(SQLS2);
    PreparedStatement stmp3=conn.prepareStatement(SQLI);

    ResultSet rset = stmt.executeQuery();
    ResultSet rset2 = stmt2.executeQuery();
    ResultSet rset3 = stmp3.executeQuery();

    // processing data
    while (rset.next()) {
      String line = String.format("id: %d, sender:%d, receiver:%d, message:%s",
          rset.getInt("id"),
          rset.getInt("user_from"),
          rset.getInt("user_to"),
          rset.getString("content"));
      System.out.println(line);
    }
//    final  String SQLI= "INSERT INTO messages (content, user_from, user_to) values(aaa,1,2)";
    while (rset2.next()) {
      String line = String.format("id: %d, sender:%d, receiver:%d, message:%s",
              rset2.getInt("id"),
              rset2.getInt("user_from"),
              rset2.getInt("user_to"),
              rset2.getString("content"));
      System.out.println(line);
    }
  }
}
