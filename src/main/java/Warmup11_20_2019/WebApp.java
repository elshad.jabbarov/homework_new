package Warmup11_20_2019;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class WebApp {
  public static void main(String[] args) throws Exception {
    Server server = new Server(8083);
    ServletContextHandler handler = new ServletContextHandler();

      handler.addServlet(Readfile.class, "/file/*");
      server.setHandler(handler);
    server.start();
    server.join();
  }
}
