package Warmup_11_25_2019;

public class Maltipication {
    static int digitat(String s, int a){
        return s.charAt(a)-'0';


    }
    static String findSum(String a, String b)
    {
        // Before proceeding further, make sure length
        // of str2 is larger.
        if (a.length() > b.length()){
            String t = a;
            a = b;
            b = t;
        }
        String str = "";

        int n1 = a.length(),
                n2 = b.length();
        int diff = n2 - n1;

        int carry = 0;

        for (int i = n1 - 1; i>=0; i--)
        {
            int sum = ((digitat(a,i)) + (digitat(b,i+diff)) + carry);
            str += (char)(sum % 10 + '0');
            carry = sum / 10;
        }

        for (int i = n2 - n1 - 1; i >= 0; i--)
        {
            int sum = ((b.charAt(i) - '0') + carry);
            str += (char)(sum % 10 + '0');
            carry = sum / 10;
        }

        if (carry > 0)
            str += (char)(carry + '0');

        // reverse resultant String
        return new StringBuilder(str).reverse().toString();

    }
    public static void main(String[] args)
    {
        String str1 = "12";
        String str2 = "198111";
        System.out.println(findSum(str1, str2));
    }


}


